<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Articles</title>
</head>
<body>
	<h3>Lista de articulos</h3>

	<table>
		<tr>
			<th>Codigo</th>
			
			<th>Nombre</th>
			
			<th>Precio</th>
		</tr>
		<c:forEach var="article" items="${ articles }">
			<tr>
				<td>${ article.code }</td>
				<td>${ article.name }</td>
				<td>${ article.price }</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>