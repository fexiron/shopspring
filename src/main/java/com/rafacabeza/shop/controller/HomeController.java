package com.rafacabeza.shop.controller;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rafacabeza.shop.model.Article;

@Controller
public class HomeController {
	
	@RequestMapping(value="/home", method=RequestMethod.GET)
	public String home(Model model) {		
		return "home";
	}
	
	@RequestMapping(value="/articles", method=RequestMethod.GET)
	public String articles(Model model) {
		
		ArrayList<Article> articles = new ArrayList<Article>();
		articles.add(new Article(1, "ART1", "Ratón", 35));
		articles.add(new Article(2, "ART2", "Teclado", 90));
		articles.add(new Article(3, "ART3", "Monitor", 200));
		articles.add(new Article(1, "ART4", "PC Gaming", 1400));
		model.addAttribute("articles", articles);

		return "articles";
	}
}
